let READCEL = {};
let XLSX = require('js-xlsx');
// import XLSX from 'js-xlsx';
(function readCel(READCEL) {
    let _cb = null;
    function creatDOM(dom = 'input') {
        let _DOM = document.createElement(dom)
        _DOM.setAttribute("id", "file")
        _DOM.setAttribute("type", "file")
        _DOM.setAttribute("accept", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        _DOM.onchange = change
        return _DOM
    }
    function selectFile(cb) {
        _cb = cb
        creatDOM().click();
    };
    // 获取上传 excel 文件
    function change(e) {
        let files = e.target.files;
        if (files.length == 0) return;
        let f = files[0];
        if (!/\.xlsx$/g.test(f.name)) {
            alert('仅支持读取xlsx格式！');
            return;
        }
        let promise = new Promise(function (resolve, reject) {
            readWorkbookFromLocalFile(f, function (workbook) {
                resolve(readWorkbook(workbook))
            })
        })
        promise.then(res => {
            _cb(res)
        })
    };
    // 获取 excel 中的内容
    function readWorkbookFromLocalFile(file, callback) {
        let reader = new FileReader();
        reader.onload = function (e) {
            let data = e.target.result;
            // excel 中的数据转为 数组
            let workbook = XLSX.read(data, { type: 'binary' });
            console.log(workbook, 'data');
            if (callback) callback(workbook);
        };
        reader.readAsBinaryString(file);
    };
    function readWorkbook(workbook) {
        let sheetNames = workbook.SheetNames; // 工作表名称集合
        let worksheet = workbook.Sheets[sheetNames[0]]; // 这里我们只读取第一张sheet
        let csv = XLSX.utils.sheet_to_csv(worksheet);
        var rows = csv.split('\n');
        rows.pop(); // 最后一行没用的
        return rows
    };
    READCEL.selectFile = selectFile;
}(typeof exports !== 'undefined' ? exports : READCEL));