const path = require('path')
module.exports = {
    mode: 'production',
    entry: './src/index.js',
    externals: ['xlsx','js-xlsx'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'readCel.js',
        library: 'readCel',
        libraryTarget: 'umd'
    }
}